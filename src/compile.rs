use super::*;

pub fn compile(c: Compile<Inp>) -> Result<(), Error> {
    let mut wrap: (&Fn(String) -> String) = &c_wrap;
    let mut comp: (&Fn(&Programm) -> String) = &to_c;

    if c.mode == Mode::ToR {
        wrap = &r_wrap;
        comp = &to_r;
    }

    get_input(c.inp)
        .and_then(Programm::parse_err)
        .map(|x| wrap(comp(&x)))
        .map(print)
}

fn c_wrap(s: String) -> String {
    let intro = "char array[300000] = {0};\nchar *ptr=array;\n";
    let func = format!("{}{}", intro, s);
    format!("{}\n{}\n{}", "int main() {", inline(func), "}")
}

fn to_c(p: &Programm) -> String {
    let mut s = String::new();
    for b in p.0.iter() {
        let r = match b {
            Basic::Shiftl => String::from("--ptr;\n"),
            Basic::Shiftr => String::from("++ptr;\n"),
            Basic::Plus => String::from("++*ptr;\n"),
            Basic::Minus => String::from("--*ptr;\n"),
            Basic::Get => String::from("*ptr=getchar();\n"),
            Basic::Print => String::from("putchar(*ptr);\n"),
            Basic::Loop(x) => format!("{}\n{}{}\n", "while (*ptr) {", inline(to_c(x)), "}"),
        };
        s += &r;
    }
    s
}

fn r_wrap(s: String) -> String {
    let intro = "
use std::io;
use std::io::{Read,Write};

fn main () {
    let mut arr:[u8;30000] = [0; 30_000];
    let mut p: usize = 0;
    
    let stdin = io::stdin();
    let stdout = io::stdout();
    let mut inp = stdin.lock();
    let mut out = stdout.lock();
     
    let mut read = || {
        let mut b = [0];
        if let Ok(x) = inp.read(&mut b) {
            if x != 1 {return 255;}
        } else {return 255;}
        b[0]
    };

    let mut print = |b: u8| {
        out.write(&[b]);
    };    
";

    format!("{}{}{}", intro, inline(s), "}")
}

fn to_r(p: &Programm) -> String {
    let mut s = String::new();
    for b in p.0.iter() {
        let r = match b {
            Basic::Shiftl => String::from("p -= 1;\n"),
            Basic::Shiftr => String::from("p += 1;\n"),
            Basic::Plus => String::from("arr[p] += 1;\n"),
            Basic::Minus => String::from("arr[p] -= 1;\n"),
            Basic::Get => String::from("arr[p] = read();\n"),
            Basic::Print => String::from("print(arr[p]);\n"),
            Basic::Loop(x) => format!("{}\n{}{}\n", "while arr[p] != 0 {", inline(to_r(x)), "}"),
        };
        s += &r;
    }
    s
}

fn inline(s: String) -> String {
    let mut res = String::new();
    for l in s.lines() {
        res += "\t";
        res += l;
        res += "\n";
    }
    res
}

fn print(s: String) -> () {
    println!("{}", s);
    ()
}
