use super::*;

pub struct ProgParse;

impl Parser<Programm> for ProgParse {
    fn parse<'l>(&self, s: &'l [u8]) -> Option<(Programm, &'l [u8])> {
        let or = Or {
            a: Box::new(BasicParse),
            b: Box::new(LoopParse),
        };
        let p = Many(Box::new(or));

        if let Some((r, s2)) = p.parse(s) {
            Some((Programm(r), s2))
        } else {
            None
        }
    }
}

struct BasicParse;
struct LoopParse;

impl Parser<Basic<Programm>> for LoopParse {
    fn parse<'l>(&self, s: &'l [u8]) -> Option<(Basic<Programm>, &'l [u8])> {
        if let Some((b'[', s1)) = Item.parse(s) {
            if let Some((p, s2)) = ProgParse.parse(s1) {
                if let Some((b']', s3)) = Item.parse(s2) {
                    Some((Basic::Loop(p), s3))
                } else {
                    None
                }
            } else {
                None
            }
        } else {
            None
        }
    }
}

impl Parser<Basic<Programm>> for BasicParse {
    fn parse<'l>(&self, s: &'l [u8]) -> Option<(Basic<Programm>, &'l [u8])> {
        if let Some((b, new)) = Item.parse(s) {
            match b {
                b'+' => Some((Basic::Plus, new)),
                b'-' => Some((Basic::Minus, new)),
                b'<' => Some((Basic::Shiftl, new)),
                b'>' => Some((Basic::Shiftr, new)),
                b',' => Some((Basic::Get, new)),
                b'.' => Some((Basic::Print, new)),
                _ => None,
            }
        } else {
            None
        }
    }
}

struct Many<A>(P<A>);

impl<A> Parser<Vec<A>> for Many<A> {
    fn parse<'l>(&self, s: &'l [u8]) -> Option<(Vec<A>, &'l [u8])> {
        let mut v: Vec<A> = Vec::new();
        let mut last = s;
        loop {
            if let Some((a, new)) = self.0.parse(last) {
                v.push(a);
                last = new;
            } else {
                break;
            }
        }
        Some((v, last))
    }
}

pub trait Parser<A> {
    fn parse<'l>(&self, &'l [u8]) -> Option<(A, &'l [u8])>;
}

type P<A> = Box<Parser<A>>;

#[allow(dead_code)]
struct Bind<'f, A: 'f, B: 'f> {
    ma: P<A>,
    f: &'f Fn(A) -> P<B>,
}

impl<'f, A, B> Parser<B> for Bind<'f, A, B> {
    fn parse<'l>(&self, s: &'l [u8]) -> Option<(B, &'l [u8])> {
        if let Some((a, new)) = self.ma.parse(s) {
            let b = (self.f)(a);
            b.parse(new)
        } else {
            None
        }
    }
}

struct IgnoreManyThen<A> {
    i: Ignore,
    a: P<A>,
}

impl<A> Parser<A> for IgnoreManyThen<A> {
    fn parse<'l>(&self, s: &'l [u8]) -> Option<(A, &'l [u8])> {
        let m = Many(Box::new(self.i.clone()));
        let a = m.parse(s);
        match a {
            None => None,
            Some((_x, s1)) => self.a.parse(s1),
        }
    }
}

#[derive(Clone)]
struct Ignore {
    tokens: Vec<u8>,
}

impl Parser<()> for Ignore {
    fn parse<'l>(&self, s: &'l [u8]) -> Option<((), &'l [u8])> {
        s.get(0).and_then(|x| {
            if self.tokens.contains(x) {
                None
            } else {
                Some(((), &s[1..]))
            }
        })
    }
}

struct Or<A> {
    a: P<A>,
    b: P<A>,
}

impl<A> Parser<A> for Or<A> {
    fn parse<'l>(&self, s: &'l [u8]) -> Option<(A, &'l [u8])> {
        let r = self.a.parse(s);
        if r.is_some() {
            r
        } else {
            self.b.parse(s)
        }
    }
}

#[allow(dead_code)]
struct Pure<A>(A)
where
    A: Clone;

impl<A> Parser<A> for Pure<A>
where
    A: Clone,
{
    fn parse<'l>(&self, s: &'l [u8]) -> Option<(A, &'l [u8])> {
        Some((self.0.clone(), s))
    }
}

#[allow(dead_code)]
fn mkpure<A>(a: A) -> P<A>
where
    A: Clone + 'static,
{
    Box::new(Pure(a))
}

struct Item;

impl Parser<u8> for Item {
    fn parse<'l>(&self, s: &'l [u8]) -> Option<(u8, &'l [u8])> {
        s.get(0).and_then(|x| Some((x.clone(), &s[1..])))
    }
}

#[cfg(test)]
mod parse_tests {
    use super::*;
    #[test]
    fn many() {
        manyfunc("test".as_bytes());
    }
    fn manyfunc(s: &[u8]) {
        let (r, _) = Many(Box::new(Item)).parse(s).expect("Many failed");
        assert_eq!(r, s);
    }

    #[test]
    fn ignore() {
        let i = Ignore { tokens: vec![b'a'] };
        assert!{i.parse("ab".as_bytes()).is_none()};
        assert!{i.parse("ba".as_bytes()).is_some()};
    }

    #[test]
    fn many_ignore() {
        let i = Ignore { tokens: vec![b'a'] };
        let many = IgnoreManyThen {
            i: i,
            a: Box::new(Item),
        };

        assert!(many.parse("bbbbbaaa".as_bytes()).is_some());

        assert_eq!(many.parse("bba".as_bytes()), Some((b'a', "".as_bytes())));
    }

}
