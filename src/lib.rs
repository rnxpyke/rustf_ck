//! # rustf_ck
//! `rustf_ck` is a brainfuck implementation in Rust.
//! A brainfuck programm can be represented with the
//! `Programm` struct and executed by any type that implements
//! the `BfMachine` trait, such as `IoSim`.

mod compile;
pub mod examples;
mod parser;

use parser::Parser;
use std::io;
use std::io::{Read, Write};

///Describes the basic operations in the Brainfuck language.
///Although this type is a generic, it should only be used
///in the context of a `Programm`, so the signature typically
///looks like `Basic<Programm>`
#[derive(PartialEq, Debug)]
pub enum Basic<A> {
    ///The `<` command
    Shiftl,
    ///The `>` command
    Shiftr,
    ///The `+` command
    Plus,
    ///The `-`command
    Minus,
    ///The `,` command
    Get,
    ///The `.` command
    Print,
    ///A Pair of `[]` with `A` inbetween
    Loop(A),
}

///Represents a brainfuck Programm
///A brainfuck Programm is a Vector of Basic commands
///so the full type is `struct Programm(Vec<Basic<Programm>>)`
#[derive(PartialEq, Debug)]
pub struct Programm(pub Vec<Basic<Programm>>);

impl Programm {
    ///Parse a programm.
    ///This function fails if the input string
    ///is not a correct brainfuck source, that is
    ///the number of `[` and `]` isn't balanced
    ///
    /// # Examples
    /// ```
    /// let prog = rustf_ck::Programm::parse(",+."); // adds one to input
    /// assert!(prog.is_some());
    /// ```
    pub fn parse(s: &str) -> Option<Programm> {
        let mut inp: Vec<u8> = s.bytes().collect();
        inp.retain(|x| "<>.,[]+-".as_bytes().contains(x));

        if let Some((r, s)) = parser::ProgParse.parse(&inp) {
            if s.is_empty() {
                return Some(r);
            }
        }
        None
    }

    ///Parse a programm into an error type
    pub fn parse_err(s: String) -> Result<Programm, Error> {
        if let Some(p) = Programm::parse(&s) {
            Ok(p)
        } else {
            Err("Could not parse the programm")
        }
    }
}

fn read_byte(r: &mut Read) -> u8 {
    let mut byte = [0];

    if let Ok(x) = r.read(&mut byte) {
        if x != 1 {
            return 255;
        }
    } else {
        return 255;
    }

    byte[0]
}

impl Sim {
    pub fn new() -> Sim {
        Sim {
            pointer: 0,
            cells: [0_u8; 30_000],
        }
    }
}

///The trait a simulator should implement for it to
///execute a brainfuck programm
pub trait BfMachine {
    ///execute a brainfuck programm
    fn execute(&mut self, prog: &Programm) {
        for ins in prog.0.iter() {
            match ins {
                Basic::Shiftl => self.left(),
                Basic::Shiftr => self.right(),
                Basic::Plus => self.plus(),
                Basic::Minus => self.minus(),
                Basic::Loop(x) => self.loopit(x),
                Basic::Print => self.print(),
                Basic::Get => self.get(),
            }
        }
    }

    ///execute a brainfuck loop
    fn loopit(&mut self, &Programm);
    ///execute the `<` command
    fn left(&mut self);
    ///execute the `>` command
    fn right(&mut self);
    ///execute the `+` command
    fn plus(&mut self);
    ///execute the `-` command
    fn minus(&mut self);
    ///execute the `,` command
    fn get(&mut self);
    ///execute the `.` command
    fn print(&mut self);
}

///A brainfuck simulator with 30000 cells.
///Doesn't do any IO, so it's not very usable,
///use IoSim instead
pub struct Sim {
    pointer: usize,
    cells: [u8; 30_000],
}

impl Sim {
    pub fn printcells(&self) -> String {
        let mut s = String::new();
        for i in 0..10 {
            s.push_str(&format!("{}", self.cells[i]));
        }
        s
    }
}

impl BfMachine for Sim {
    fn left(&mut self) {
        self.pointer -= 1;
    }
    fn right(&mut self) {
        self.pointer += 1;
    }
    fn plus(&mut self) {
        self.cells[self.pointer] += 1;
    }
    fn minus(&mut self) {
        self.cells[self.pointer] -= 1;
    }
    fn get(&mut self) {}
    fn print(&mut self) {}
    fn loopit(&mut self, p: &Programm) {
        while self.cells[self.pointer] != 0 {
            self.execute(p);
        }
    }
}

///A brainfuck simulator with IO. Uses the normal
///`Sim` inside, as well as a input and output channel
pub struct IoSim<'a> {
    data: Sim,
    inp: &'a mut Read,
    out: &'a mut Write,
}

impl<'a> IoSim<'a> {
    ///Create a new `IoSim` by supplying the assosciated input/output channels
    ///(usually stdin/stdout)
    ///
    /// # Examples
    /// ```
    /// use std::io;
    ///
    /// let stdout = io::stdout();
    /// let mut out = stdout.lock();
    ///
    /// let stdin = io::stdin();
    /// let mut inp = stdin.lock();
    ///
    /// let mut sim = rustf_ck::IoSim::new(&mut inp, &mut out);
    ///  
    /// ```
    pub fn new(inp: &'a mut Read, out: &'a mut Write) -> IoSim<'a> {
        IoSim {
            data: Sim::new(),
            inp: inp,
            out: out,
        }
    }

    ///Run a Programm with typical stdin/out channels
    pub fn run(p: &Programm) {
        let stdout = io::stdout();
        let mut out = stdout.lock();

        let stdin = io::stdin();
        let mut inp = stdin.lock();

        let mut sim = IoSim::new(&mut inp, &mut out);
        sim.execute(p);
    }
}

impl<'a> BfMachine for IoSim<'a> {
    fn left(&mut self) {
        self.data.left()
    }
    fn right(&mut self) {
        self.data.right()
    }
    fn plus(&mut self) {
        self.data.plus()
    }
    fn minus(&mut self) {
        self.data.minus()
    }
    fn loopit(&mut self, p: &Programm) {
        while self.data.cells[self.data.pointer] != 0 {
            self.execute(p);
        }
    }
    fn get(&mut self) {
        self.data.cells[self.data.pointer] = read_byte(self.inp);
    }
    fn print(&mut self) {
        self.out.write(&[self.data.cells[self.data.pointer]]);
        self.out.flush();
    }
}

///Describes the possible input types for this programm, a pure string,
///or a filename
pub enum Inp {
    File(String),
    Pure(String),
}

///Describes the compilation config
///A is the input type
///the out field descripes an optional output name
///and the mode descripes the compilation target
pub struct Compile<A> {
    pub inp: A,
    pub out: Option<String>,
    pub mode: Mode,
}

impl Compile<Inp> {
    ///Compiles a brainfuck programm.
    pub fn compile(self) -> Result<(), Error> {
        compile::compile(self)
    }
}

///Describes the compilation modes. Targets are C, Rust and machine code
#[derive(PartialEq, Debug)]
pub enum Mode {
    ToC,
    ToR,
    Asm,
}

///The rustfuck error type
pub type Error = &'static str;

impl Inp {
    ///Returns the programm based on the input type.
    ///for a file input, get tries to read the file
    pub fn get(self) -> Result<String, Error> {
        get_input(self)
    }
}

fn get_input(i: Inp) -> Result<String, Error> {
    match i {
        Inp::Pure(x) => Ok(x),
        Inp::File(x) => match std::fs::read_to_string(x) {
            Ok(s) => Ok(s),
            _ => Err("Error with file"),
        },
    }
}
