extern crate rustf_ck;

use rustf_ck::*;

use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();
    if let Err(e) = parse_config(&args).and_then(run_config) {
        eprintln!("Error: {}\n{}", e, USAGE);
        process::exit(1);
    }
}

const USAGE:&'static str = "Usage:\nrustf_ck EXPR\t run programm\nrustf_ck -f FILENAME\trun file
rustf_ck -c[cr][f]\t compile programm and print result to stdout\n\t\t\t use -cc for C and -cr for rust
rustf_ck -h \t show this help";

enum Config {
    Help,
    Run(Inp),
    Comp(Compile<Inp>),
}

fn parse_config(args: &Vec<String>) -> Result<Config, Error> {
    if args.len() >= 3 {
        if args[1] == "-f" {
            return Ok(Config::Run(Inp::File(args[2].clone())));
        }
        let p = Inp::Pure(args[2].clone());
        let f = Inp::File(args[2].clone());
        let mut comp = Compile {
            inp: p,
            out: None,
            mode: Mode::Asm,
        };
        match args[1].as_str() {
            "-c" => {}
            "-cc" => comp.mode = Mode::ToC,
            "-cr" => comp.mode = Mode::ToR,
            "-cf" => comp.inp = f,
            "-ccf" => {
                comp.mode = Mode::ToC;
                comp.inp = f
            }
            "-crf" => {
                comp.mode = Mode::ToR;
                comp.inp = f
            }
            _ => return Err("Unknown Parameter"),
        }
        if let Some(x) = args.get(3) {
            comp.out = Some(x.to_string());
        }
        return Ok(Config::Comp(comp));
    }

    if let Some(x) = args.get(1) {
        if x.as_str() == "-h" {
            Ok(Config::Help)
        } else {
            Ok(Config::Run(Inp::Pure(x.clone())))
        }
    } else {
        Err("expected more arguments")
    }
}

fn run_config(c: Config) -> Result<(), Error> {
    match c {
        Config::Help => {
            printhelp();
            Ok(())
        }
        Config::Run(x) => x
            .get()
            .and_then(Programm::parse_err)
            .map(|x| IoSim::run(&x)),
        Config::Comp(x) => x.compile(),
    }
}

fn printhelp() {
    println!("{}", USAGE);
}
