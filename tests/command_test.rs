extern crate rustf_ck;

use rustf_ck::*;


#[test]
fn parse_basic() {
    let p = Programm::parse("+-><,.");
    assert!(p.is_some());
}

#[test] 
fn parse_ignore() {
    let empty = Programm::parse("");
    let ignore = Programm::parse("a");
    assert_eq!(empty, ignore);
}
#[test] 
fn parse_ignore_2() {
    let empty = Programm::parse("");
    let ignore = Programm::parse("et");
    assert_eq!(empty, ignore);
}

#[test]
fn parse_ignore_long() {
    let normal = Programm::parse("++[->+<]");
    let other = Programm::parse("a+b+\n'c[-\t#>+:<]c");
    assert_eq!(normal,other);
}

#[test]
fn parse_add_two() {
    assert!(Programm::parse("[->+<]").is_some())
}

#[test]
fn parse_helloworld() {
    assert!(Programm::parse(examples::HELLO).is_some())
}

#[test]
fn parse_invalid() {
    assert_eq!(Programm::parse("["), None);
    assert_eq!(Programm::parse("]"), None);
}

