# rustf_ck

this is a [brainfuck interpreter](https://en.wikipedia.org/wiki/Brainfuck) written in rust.

## Building


This is a rust (only) project, so the build steps are fairly simple. Install the `cargo` tool through your package manager or use the [official installation instructions](https://www.rust-lang.org/tools/install), then just run the following:

```bash
# build the project
cargo build

# run tests
cargo test

# run the project
cargo run
```


## Usage
```
rustf_ck EXPR           run programm
rustf_ck -f FILENAME    run file
rustf_ck -c[cr][f]      compile programm and print result to stdout
                        use -cc for C and -cr for rust
rustf_ck -h             show this help
```
